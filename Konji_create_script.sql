use NasaBaza;

drop table if exists Konji;
drop table if exists Ergele;
create table Ergele(
	id int auto_increment,
	kapacitet int,
    adresa varchar(30),
    primary key (id)
);

create table Konji (
	ergela int not null,
	jedinstveni_kljuc int auto_increment,
    ime varchar(20),
    tezina int,
    primary key (jedinstveni_kljuc),
    foreign key (ergela) references Ergele(id)
);

insert into Ergele(kapacitet, adresa) values (10, "Lupiceva");
insert into Ergele(kapacitet, adresa) values (18, "Perina");

insert into Konji(ime, tezina, ergela) values ("Roko", 400, 1);
insert into Konji(ime, tezina, ergela) values ("Luka", 450, 2);
insert into Konji(ime, tezina, ergela) values ("Luka", 450, 1);
insert into Konji(ime, tezina, ergela) values ("Luka", 769, 1);

select * from Konji;
select * from Ergele;

select distinct Ergele.adresa
from Konji join Ergele
on Konji.ergela = Ergele.id
where Konji.ime like "Luka"